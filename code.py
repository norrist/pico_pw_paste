import os
import time

import board
import digitalio
import usb_hid

# https://github.com/adafruit/Adafruit_CircuitPython_Bundle
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

with open("pw.txt", "r") as pw_file:
    pw = pw_file.read()

a_keys_pressed = [Keycode.A, pw]
control_key = Keycode.SHIFT

# The keyboard object!
time.sleep(1)  # Sleep for a bit to avoid a race condition on some systems
keyboard = Keyboard(usb_hid.devices)
keyboard_layout = KeyboardLayoutUS(keyboard)  # We're in the US :)


def blink_count(i, delay):
    for _ in range(i):
        led.value = True
        time.sleep(delay)
        led.value = False
        time.sleep(delay)

    time.sleep(1)


def countdown_and_send():
    for i in reversed(range(6)):
        print(i)
        blink_count(i, delay=0.2)

    print(f"Sending {a_keys_pressed} as keystrokes")
    for key in a_keys_pressed:
        if isinstance(key, str):  # If it's a string...
            keyboard_layout.write(key)  # ...Print the string


while True:
    print("Hello, CircuitPython!")

    try:
        os.stat("send_keys")
        countdown_and_send()

    except OSError:
        blink_count(1, 1)
        print("touch send_keys to enable keypresses")
