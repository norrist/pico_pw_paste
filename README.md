# Pi Pico PW
Use a raspberry pi pico to input passwords into website fields that disable paste.

## Setup 
### Install circuit python
- https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/circuitpython

### Bundle
- `https://github.com/adafruit/Adafruit_CircuitPython_Bundle` -> Releases
- Download `adafruit-circuitpython-bundle-py-20231219.zip` or current version.
- Create a `lib` directory on the circuit python drive.
- copy the directory `lib/adafruit_hid` from the zip to `lib` on the circuit python drive.


## How to use
- Plug in the pico to your PC and mount the drive
- The LED will blink - 1 second on and 1 second off
- Save the password you want to input as keystrokes in the file `pw.txt`
- When you are ready for the pico to do the typing, create the file `send_keys`
- The LED's will blink fast 5 times, then 4 times, ...
- Wait for the LED flashed to count down.
- The pico will send the keystrokes and restart the countdown.
- Remove the file `send_keys` so stop the input.

## Adapted from Ada fruit circuit python examples
- [Keyboard Example](https://github.com/adafruit/Adafruit_Learning_System_Guides/blob/main/Introducing_CircuitPlaygroundExpress/CircuitPlaygroundExpress_HIDKeyboard/code.py)
