# SPDX-FileCopyrightText: 2018 Kattni Rembor for Adafruit Industries
#
# SPDX-License-Identifier: MIT

"""CircuitPython Essentials HID Keyboard example"""
import time

import usb_hid
from adafruit_circuitplayground import cp
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

a_keys_pressed = [Keycode.A, "aaaabbbbccccdddd@@"]
b_keys_pressed = [Keycode.A, "eeeeffffgggghhhh@@"]


control_key = Keycode.SHIFT

time.sleep(1)  # Sleep for a bit to avoid a race condition on some systems
keyboard = Keyboard(usb_hid.devices)
keyboard_layout = KeyboardLayoutUS(keyboard)  # We're in the US :)


print("Waiting for key pin...")

while True:
    if cp.button_a:
        print("A")
        for key in a_keys_pressed:
            if isinstance(key, str):  # If it's a string...
                keyboard_layout.write(key)  # ...Print the string
    elif cp.button_b:
        print("B")
        for key in b_keys_pressed:
            if isinstance(key, str):  # If it's a string...
                keyboard_layout.write(key)  # ...Print the string

    time.sleep(0.1)
